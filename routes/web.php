<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/masproductos', function () {
    return view('producto');
});

Route::get('/', function () {
    return view('welcomeInicio');
});

Route::get('/misproductos', function () {
    return view('producto');
});
Route::get('/nuevoproductos', function () {
    return view('producto');
});

Route::resource('articulos','App\Http\Controllers\ArticuloController');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
